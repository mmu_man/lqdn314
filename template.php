<?php

/**
 * @file LQDN theme template.php.
 * Override or insert variables into the templates.
 */

/**
 * Implements hook_preprocess_page().
 */
function lqdn314_preprocess_page(&$vars) {
  // dsm($vars);

  // Top navigation
  if (!empty($vars['primary_links'])) {
    $vars['top_navigation'] = theme('links', $vars['primary_links'], array('class' => 'links primary-links'));
  }

  // Language switcher
  $block = module_invoke('locale', 'block', 'view', '0');
  $vars['lang_switcher'] = $block['content'];

  // Nous soutenir, Participer
  if (!empty($vars['secondary_links'])) {
    $vars['header_navigation'] = theme('links', $vars['secondary_links'], array('class' => 'links secondary-links'));
  }

  // Share links
  $block = module_invoke('lqdn_share_links', 'block', 'view', '0');
  $vars['share_links'] =  $block['content'];

  // LQDN dossiers
  $vars['dossiers_menu'] = module_invoke('lqdn_dossiers', 'block', 'view', '1');

  // Move JS files "$scripts" to page bottom for perfs/logic.
  // Add JS files that *needs* to be loaded in the head in a new "$head_scripts" scope.
  // $path = drupal_get_path('theme', 'lqdn314');
  // drupal_add_js($path . '/js/modernizr.custom.55191.js', array('scope' => 'head_scripts', 'weight' => -1, 'preprocess' => FALSE));
  $vars['head_scripts'] = drupal_get_js('head_scripts');

  // Footer message, licence, date, etc.
  $vars['lqdn_footer_message'] = theme('lqdn_footer_message', array());

  // Bottom etc.
  $vars['bottom'] = theme('lqdn_bottom', array());

  // Adding a CSS class in the body to identify panels.
  // Based on http://drupal.org/node/1062390
  $vars['menu_item'] = menu_get_item();
  switch ($vars['menu_item']['page_callback']) {
  case 'page_manager_page_execute':
  case 'page_manager_node_view':
  case 'page_manager_contact_site':
    $vars['body_classes'] .= ' page-panels';
    break;
  }
}

function lqdn314_node_submitted($node) {
  return t('Submitted on @datetime',
           array('@datetime' => format_date($node->created)));
}

/**
 * Implements hook_theme().
 */
function lqdn314_theme($existing, $type, $theme, $path) {
  return array(
    'search_theme_form' => array(
      'arguments' => array('form' => NULL),
    ),
    'lqdn_footer_message' => array(
      'arguments' => array('vars' => NULL),
      'template'  => 'lqdn-footer-message',
      'path' => $path . '/templates',
    ),
    'lqdn_bottom' => array(
      'arguments' => array('vars' => NULL),
      'template'  => 'lqdn-bottom',
      'path' => $path . '/templates',
    ),
  );
}

function lqdn314_search_theme_form($form) {
  $text = t('Search...');
  $form['search_theme_form']['#title'] = $text;
  $form['search_theme_form']['#attributes'] = array(
    'title' => $text,
    'placeholder' => $text,
  );
  return drupal_render($form);
}

/**
 * Implements hook_preprocess_template().
 */
function lqdn314_preprocess_lqdn_footer_message(&$vars) {
  global $user;

  $themepath = drupal_get_path('theme', 'lqdn314');
  $logo = theme('image', "{$themepath}/img/lqdn-logo_24x24.png", 'La Quadrature du Net');
  $attributes = array('rel' => 'nofollow');
  $options = array('attributes' => $attributes, 'html' => TRUE);
  $loginpath = empty($user->uid) ? 'user' : 'logout';
  $vars['logo_login'] = l($logo, $loginpath, $options);
  $vars['title'] = 'La Quadrature du Net';
  $year = format_date(time(), 'custom', 'Y');
  $vars['date'] = "2008 - {$year}";
  $img = theme('image', "{$themepath}/img/cc-by-sa-3.0.png", 'Creative Commons License (CC-BY-SA-3.0)');
  $vars['cc'] = l($img, 'http://creativecommons.org/licenses/by-sa/3.0/', array(
    'html' => TRUE,
    'attributes' => array('rel' => 'licence'),
  ));
}

/**
 * Dummy form, devra etre deplacé/remplacé dans un module.
 */
function lqdn314_newsletterform($form_state) {
  $text = t('mail@example.com');
  $form['mail'] = array(
    '#type' => 'textfield',
    '#title' => t('Join our newsletter'),
    '#default_value' => '',
    '#size' => 25,
    '#maxlength' => 100,
    '#description' => '',
    '#attributes' => array(
      'title' => $text,
      'placeholder' => $text,
    ),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Subscribe'),
  );
  $form['#action'] = '/mailman_sub.php';
  return $form;
}

/**
 * Implements hook_preprocess_template().
 */
function lqdn314_preprocess_lqdn_bottom(&$vars) {
  $vars['newsletter_form'] = drupal_get_form('lqdn314_newsletterform');
  $vars['feeds']['news'] = l(t('RSS News'), 'rss.xml');
  $vars['feeds']['review'] = l(t('RSS Press review'), 'press-review/feed');
  $vars['social']['twitter'] = l(t('Twitter'), 'https://twitter.com/laquadrature');
  $vars['social']['identica'] = l(t('Identi.ca'), 'https://identi.ca/laquadrature');
}
function lqdn314_preprocess_panels_pane(&$vars) {
  $content = $vars['output'];
  $vars['title'] = !empty($content->title) ? t('@title', array('@title' => $content->title)) : '';
}

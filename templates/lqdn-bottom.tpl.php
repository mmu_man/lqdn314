<div id="bottom-newsletter">
  <span class="icon contact"></span>
  <?php print $newsletter_form; ?>
</div>
<div id="bottom-feeds">
  <p>
    <span class="icon rss"></span>
    <span><?php print $feeds['news']; ?></span>
    <span>|</span>
    <span><?php print $feeds['review']; ?></span>
  </p>
</div>
<div id="bottom-social">
  <p>
    <span class="icon twitter"></span>
    <span><?php print $social['twitter']; ?></span>
  </p>
<!--
  <p>
    <span class="icon identica"></span>
    <span><?php print $social['identica']; ?></span>
  </p>
-->
</div>
